# Ressources associées à la présentation d'A* lors  de la journée NSI 2 (académie de Reims)


**Contenu**

- `astar_nsi.pdf` : présentation
- `astar_nsi.ipynb` : _notebook_ d'accompagnement

**À propos**

- _Journée de formation NSI_
- _Lycée Jean Talon, Châlons-en-Champagne, 1 avril 2022_
- _Organisation : Michaël Balandier, Bertrand Lemaître, Régis Queruel, Pascal Thérèse_

